package com.example.tarea2pidm;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class ListResult extends AppCompatActivity {
    ListView listaresultados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_result);
        listaresultados = (ListView) findViewById(R.id.lstpersonas);


        ArrayList<String> lstesultados = new ArrayList<>();
        Bundle results = getIntent().getExtras();
        Integer d1 = results.getInt("Resultado");
        lstesultados.add("el resultado es : "+d1);
        //lstesultados.add(44);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,lstesultados);


        listaresultados.setAdapter(adapter);

        backCalculadora();
    }

    private void backCalculadora(){
        Button btnback = (Button) findViewById(R.id.btnback);
        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


}