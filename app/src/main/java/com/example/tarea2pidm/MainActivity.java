package com.example.tarea2pidm;

import static java.sql.DriverManager.println;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.sql.Array;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Spinner mSpinner;
    String elemento;
    Button btncalcular;
    EditText txtn1, txtn2;
    int producto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSpinner = (Spinner) findViewById(R.id.mSpinner);
        ArrayList<String> elementos =  new ArrayList<>();

        elementos.add("Suma");
        elementos.add("Resta");
        elementos.add("Multiplicacion");
        elementos.add("Division");

        ArrayAdapter adp = new ArrayAdapter(MainActivity.this,
                android.R.layout.simple_spinner_dropdown_item, elementos);

        mSpinner.setAdapter(adp);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               elemento = (String) mSpinner.getAdapter().getItem(i);

               Toast.makeText(MainActivity.this, "Seleccionaste: " + elemento,
                       Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        btncalcular =(Button)findViewById(R.id.btncalcular);
        btncalcular.setOnClickListener(this);

        txtn1=(EditText)findViewById(R.id.textnum1);
        txtn2=(EditText)findViewById(R.id.textnum2);

        ListResult();

    }

    public void Calcular()
    {

        String n1cad=txtn1.getText().toString();
        int n1=Integer.parseInt(n1cad);

        String n2cad=txtn2.getText().toString();
        int n2=Integer.parseInt(n2cad);

        if(elemento=="Suma"){
            producto =n1+n2;
        }
        else if( elemento =="Resta"){
            producto =n1-n2;
        }
        else if ( elemento == "Multiplicacion"){
            producto = n1*n2;
        }
        else {
            producto = n1/n2;
        }
        //resultados.add(producto);

        Toast.makeText(getApplicationContext(),"El producto es :" +producto,Toast.LENGTH_LONG).show();




    }


    private void ListResult()
    {
        Button btnListo = (Button) findViewById(R.id.btnlisto);
        btnListo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ListResult.class);
                i.putExtra("Resultado",producto);
                startActivity(i);
                //startActivity(new Intent(MainActivity.this, ListResult.class));

            }
        });
    }


    @Override
    public void onClick(View v) {

            Calcular();

    }
}